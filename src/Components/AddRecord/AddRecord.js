import React, { useState } from 'react';
import * as _ from 'lodash';
import './AddRecord.css';
import Swal from 'sweetalert2';
const { ipcRenderer } = window.require('electron');


export default function AddRecord({ selectedYear, refresh, close }) {
    const [stream, setStream] = useState('');
    const [sub1, setSub1] = useState([]);
    const [sub2, setSub2] = useState([]);
    const [sub3, setSub3] = useState([]);
    const [selectedSub1, setSelectedSub1] = useState('');
    const [selectedSub2, setSelectedSub2] = useState('');
    const [selectedSub3, setSelectedSub3] = useState('');
    const [name, setName] = useState('');
    const [index, setIndex] = useState('');
    const [attempt, setAttempt] = useState('');
    const [grade1, setGrade1] = useState('');
    const [grade2, setGrade2] = useState('');
    const [grade3, setGrade3] = useState('');

    function controlSub(value, subNumber) {
        setStream(value);
        switch (subNumber) {
            case 1:
                controlSub1(value)
                break;
            case 2:
                controlSub2(value)
                break;
            case 3:
                controlSub3(value)
                break;
            default:
                break;
        }

    }

    function controlSub1(value) {
        switch (value) {
            case 'MATHS':
                setSub1([
                    'C_MATHS', 'CHEMISTRY', 'PHYSICS', 'ICT'
                ]);
                break;
            case 'SCIENCE':
                setSub1([
                    'BIO', 'CHEMISTRY', 'PHYSICS', 'AGRICULTURE'
                ]);
                break;
            case 'COMMERCE':
                setSub1(['ACCOUNTING', 'ECONOMICS', 'BUSINESS_STUDIES', 'STATISTICS',
                    'GEOGRAPHY', 'POLITICAL_SCIENCE', 'HISTORY(INDIAN)', 'AGRICULTURAL_SCIENCE', 'ICT', 'LOGIC_AND_THE_SCIENTIFIC_METHOD']);
                break;
            case 'ART':
                setSub1(['ECONOMICS', 'GEOGRAPHY', 'HISTORY(INDIAN)', 'HOME_ECONOMICS_SCIENCE',
                    'POLITICAL_SCIENCE', 'LOGIC_AND_THE_SCIENTIFIC_METHOD', 'ACCOUNTING', 'AGRICULTURAL_SCIENCE',
                    'ICT', 'COMMUNICATION_&_MEDIA_STUDIES', 'BUDDHIST_CIVILIZATION', 'CHRIST', 'ART', 'DANCING(EASTERN)',
                    'ORIENTAL_MUSIC', 'DRAMA']);
                break;

            default:
                break;
        }
    }

    function controlSub2(value) {
        setSub1([value]);
        setSelectedSub1(value);

        switch (stream) {
            case 'MATHS':
                setSub2([...sub1.filter(e => e !== value)]);
                break;
            case 'SCIENCE':
                setSub2([...sub1.filter(e => e !== value)]);
                break;
            case 'COMMERCE':
                setSub2([...sub1.filter(e => e !== value)]);
                break;
            case 'ART':
                setSub2([...sub1.filter(e => e !== value)]);
                break;

            default:
                break;
        }
    }
    function controlSub3(value) {
        setSub2([value])
        setSelectedSub2(value);

        switch (stream) {
            case 'MATHS':
                setSub3([...sub2.filter(e => e !== value)]);
                break;
            case 'SCIENCE':
                setSub3([...sub2.filter(e => e !== value)]);
                break;
            case 'COMMERCE':
                setSub3([...sub2.filter(e => e !== value)]);
                break;
            case 'ART':
                setSub3([...sub2.filter(e => e !== value)]);
                break;

            default:
                break;
        }
    }
    function setSub3Values(value) {
        setSub3([value])
        setSelectedSub3(value);
    }
    function validForm(event) {
        return (!_.isEmpty(name)
            && !_.isEmpty(attempt)
            && !_.isEmpty(stream)
            && !_.isEmpty(selectedSub1)
            && !_.isEmpty(selectedSub2)
            && !_.isEmpty(selectedSub3)
            && !_.isEmpty(index) && !_.isEmpty(selectedYear)
        ) ? true : false

    }
    async function save(event) {
        if (!validForm()) {
            Swal.fire('Fill all the fields before submitting', '', 'warning');
            resetForm();
        } else {
            ipcRenderer.send('createRecord', {
                year: selectedYear,
                name,
                index,
                attempt,
                stream,
                sub1: selectedSub1,
                sub2: selectedSub2,
                sub3: selectedSub3,
                grade1,
                grade2,
                grade3,
                universitySelected: (grade1 !== 'F' && grade2 !== 'F' && grade3!=='F')?true:false
            });
            ipcRenderer.on('createResponse', (event, data) => {
                refresh();
            });

            resetForm();

        }
        event.preventDefault();
    }
    function resetForm(event) {

        setName('');
        setIndex('');
        setAttempt('');
        setStream('');
        setSelectedSub1('');
        setSelectedSub2('');
        setSelectedSub3('');
        setSub1([]); setSub2([]); setSub3([]);
        setGrade1([]); setGrade2([]); setGrade3([]);


    }
    function resetForm2(event) {

        setName('');
        setIndex('');
        setAttempt('');
        setStream('');
        setSelectedSub1('');
        setSelectedSub2('');
        setSelectedSub3('');
        setSub1([]); setSub2([]); setSub3([]);
        setGrade1([]); setGrade2([]); setGrade3([]);
        event.preventDefault()
    }
    return (
        <div className='container add-r'>
            <h1 className="flex justify-center p-3 font-bold">Add a new Record Here for </h1> <b>{selectedYear}</b>
            <span onClick={()=>{close()}} className='close'>X</span>
            <form className="w-full max-w-lg form-el">
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Full Name
                          </label>
                    </div>
                    <div className="md:w-2/3">
                        <input value={name} onChange={(e) => setName(e.target.value)} className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="inline-full-name" type="text" />
                    </div>
                </div>

                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Index
                          </label>
                    </div>
                    <div className="md:w-2/3">
                        <input value={index} onChange={(e) => setIndex(e.target.value)} className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="inline-full-name" type="text" />
                    </div>
                </div>

                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label type='number' className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Attempt
                          </label>
                    </div>
                    <div className="md:w-2/3">
                        <input value={attempt} onChange={(e) => setAttempt(e.target.value)} className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="inline-full-name" type="text" />
                    </div>
                </div>

                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Stream
                          </label>
                    </div>
                    <div className="relative">
                        <select value={stream} onChange={(e) => controlSub(e.target.value, 1)} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            <option value='MATHS'>Math</option>
                            <option value='SCIENCE' >Bio Science</option>
                            <option value='ART'>Art</option>
                            <option value='COMMERCE'>Commerce</option>
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Subject 1:
                          </label>
                    </div>
                    <div className="relative">
                        <select onChange={(e) => { controlSub2(e.target.value) }} value={selectedSub1} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            {
                                sub1.map(el => <option value={el} key={el}> {el} </option>)
                            }
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Grade1
                          </label>
                    </div>
                    <div className="relative">
                        <select value={grade1} onChange={(e) => setGrade1(e.target.value)} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            <option value='A'>A</option>
                            <option value='B' >B</option>
                            <option value='C'>C</option>
                            <option value='S'>S</option>
                            <option value='F'>F</option>
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Subject 2:
                          </label>
                    </div>
                    <div className="relative">
                        <select onChange={(e) => { controlSub3(e.target.value) }} value={selectedSub2} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            {
                                sub2.map(el => <option value={el} key={el}> {el} </option>)
                            }
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Grade2
                          </label>
                    </div>
                    <div className="relative">
                        <select value={grade2} onChange={(e) => setGrade2(e.target.value)} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            <option value='A'>A</option>
                            <option value='B' >B</option>
                            <option value='C'>C</option>
                            <option value='S'>S</option>
                            <option value='F'>F</option>
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Subject 3:
                          </label>
                    </div>
                    <div className="relative">
                        <select onChange={e => setSub3Values(e.target.value)} value={selectedSub3} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            {
                                sub3.map(el => <option value={el} key={el}> {el} </option>)
                            }
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <div className="md:flex md:items-center mb-6">
                    <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" htmlFor="inline-full-name">
                            Grade3
                          </label>
                    </div>
                    <div className="relative">
                        <select value={grade3} onChange={(e) => setGrade3(e.target.value)} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                            <option>Select</option>
                            <option value='A'>A</option>
                            <option value='B' >B</option>
                            <option value='C'>C</option>
                            <option value='S'>S</option>
                            <option value='F'>F</option>
                        </select>
                        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                        </div>
                    </div>
                </div>
                <button className="bg-blue-100 hover:bg-blue-400 mb-10 mt-10 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded mr-8" onClick={save} >Add record</button>
                <button className="bg-blue-100 hover:bg-green-700 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded" onClick={resetForm2} >Reset form</button>
            </form>
        </div>
    )
}

