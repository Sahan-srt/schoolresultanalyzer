import React, { useState } from 'react'
import HomeComponent from '../HomeComponent/HomeComponent';
import SettingsComponent from '../Settings/SettingsComponent';

export default function HeaderComponent() {
    const [selectedTab, changeTab] = useState(1)
    const noneSelectedCss = 'bg-white inline-block py-2 px-4 text-blue-500 hover:text-blue-800 font-semibold';
    const selectedCss = 'bg-indigo-200 inline-block border-l border-t border-r rounded-t py-2 px-4 text-blue-700 font-semibold ';
    let showLayout = null;
    switch (selectedTab) {
        case 1:
            showLayout = <HomeComponent />;
            break;
        case 2:
            showLayout = <SettingsComponent />;
            break;
        default:
            break;
    }
    return (
        <div className='main-container'>
            <ul className="fixed flex w-full  items-center justify-center flex-wrap mt-1 top-0  animated ">
                <li onClick={() => { changeTab(1) }} className="-mb-px mr-1">
                    <a className={selectedTab === 1 ? selectedCss : noneSelectedCss} href="#">Home Screen</a>
                </li>
                <li onClick={() => { changeTab(2) }} className="mr-1">
                    <a className={selectedTab === 2 ? selectedCss : noneSelectedCss} href="#">Settings Page</a>
                </li>
            </ul>

            {showLayout}

        </div>

    )
}

