import React, { Component } from 'react';
import '../AddRecord/AddRecord.css';
import './Analytics.css';
import * as _ from 'lodash';

export default class Analytics extends Component {
  constructor(props) {
    super(props);
    const { dataSet = [], selectedYear } = props;
    this.state = {
      results: dataSet,
      passedCount: null,
      passedCount: null,
      selectedStream: null,
      selectedSubject: null,
      selectedStreamData: {},
      tableHeads: null,
      tableRows: null,
    };
  }

  resetState() {
    this.setState({
      passedCount: null,
      passedCount: null,
      selectedStream: null,
      selectedSubject: null,
      selectedStreamData: {},
    });
  }
  componentWillMount() {
    this.findPassCount();
    this.findFailedCount();
    this.calculateStreamResults(this.state.selectedStream);
  }

  findPassCount() {
    //console.log(this.state.results);
    const passedArray = this.state.results.filter((e) => {
      if (e.grade1 !== 'F' && e.grade2 !== 'F' && e.grade3 !== 'F') return e;
    });
    this.setState({ passedCount: passedArray.length });
  }

  findFailedCount() {
    const failedArray = this.state.results.filter((e) => {
      if (e.grade1 === 'F' || e.grade2 === 'F' || e.grade3 === 'F') return e;
    });
    this.setState({ failedCount: failedArray.length });
  }

  calculateStreamResults(stream) {
    this.setState({ selectedStream: stream });
    let selectedStreamDataSet = {};
    switch (stream) {
      case 'MATHS': {
        selectedStreamDataSet = {
          subjects: {
            C_MATHS: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            CHEMISTRY: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            PHYSICS: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ICT: { A: 0, B: 0, C: 0, S: 0, F: 0 },
          },
        };
        break;
      }
      case 'SCIENCE':
        selectedStreamDataSet = {
          subjects: {
            BIO: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            CHEMISTRY: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            PHYSICS: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            AGRICULTURE: { A: 0, B: 0, C: 0, S: 0, F: 0 },
          },
        };
        break;
      case 'COMMERCE':
        selectedStreamDataSet = {
          subjects: {
            ACCOUNTING: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ECONOMICS: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            BUSINESS_STUDIES: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            STATISTICS: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            GEOGRAPHY: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            POLITICAL_SCIENCE: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            'HISTORY(INDIAN)': { A: 0, B: 0, C: 0, S: 0, F: 0 },
            AGRICULTURAL_SCIENCE: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            LOGIC_AND_THE_SCIENTIFIC_METHOD: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ICT: { A: 0, B: 0, C: 0, S: 0, F: 0 },
          },
        };

        break;
      case 'ART':
        selectedStreamDataSet = {
          subjects: {
            ECONOMICS: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            GEOGRAPHY: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            'HISTORY(INDIAN)': { A: 0, B: 0, C: 0, S: 0, F: 0 },
            HOME_ECONOMICS_SCIENCE: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            POLITICAL_SCIENCE: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            LOGIC_AND_THE_SCIENTIFIC_METHOD: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ACCOUNTING: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            AGRICULTURAL_SCIENCE: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            'COMMUNICATION_&_MEDIA_STUDIES': { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ICT: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            BUDDHIST_CIVILIZATION: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            CHRIST: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ART: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            'DANCING(EASTERN)': { A: 0, B: 0, C: 0, S: 0, F: 0 },
            ORIENTAL_MUSIC: { A: 0, B: 0, C: 0, S: 0, F: 0 },
            DRAMA: { A: 0, B: 0, C: 0, S: 0, F: 0 },
          },
        };
        break;

      default:
        break;
    }
    let streamPasses = 0;
    let streamFails = 0;
    this.state.results.forEach((result) => {
      if (stream === result.stream) {
        const { sub1, sub2, sub3, grade1, grade2, grade3, attempt } = result;
 
        if (grade1!=='F' && grade2!=='F' && grade3!=='F') {
          streamPasses++;
        } else {
          streamFails++;
        }
        // console.log(selectedStreamDataSet.subjects[sub1], sub1);
        Object.entries(selectedStreamDataSet.subjects).map(([key, value]) => {
          switch (key) {
            case sub1:
              value[grade1]++;
              break;
            case sub2:
              value[grade2]++;
              break;
            case sub3:
              value[grade3]++;
              break;

            default:
              break;
          }
        });
      }
    });
    this.setState({
      selectedStreamData: {
        subjects: selectedStreamDataSet.subjects,
        streamPasses,
        streamFails
      },
      tableHeads:
        this.state.tableHeads != null ? (
          Object.entries(selectedStreamDataSet.subjects).map(([key, value]) => {
            return (
              <th className='px-4 py-2' key={key}>
                {key}
              </th>
            );
          })
        ) : (
          <th className='px-4 py-2'></th>
        ),
      tableRows:
        this.state.tableRows != null ? (
          Object.entries(selectedStreamDataSet.subjects).map(([key, value]) => {
            return (
              <td key={key} className='border px-2 py-2'>
                <ul>
                  <li>A: {value.A}</li>
                  <li>B: {value.B}</li>
                  <li>C: {value.C}</li>
                  <li>S: {value.S}</li>
                  <li>F: {value.F}</li>
                </ul>
              </td>
            );
          })
        ) : (
          <td className=' px-4 py-2'></td>
        ),
    });
  }

  render() {
    return (
      <div className='container c bg-blue-800 '>
        <h1>Analytic View</h1>
        <span
          onClick={() => {
            this.props.close();
          }}
          className='close'
        >
          X
        </span>

        <div className='total-value'>
          <h3 className='py-3'>Total Passes: {this.state.passedCount} </h3>
          <h3>Total fails: {this.state.failedCount}</h3>

          <div className='relative stream-a'>
            <select
              value={
                this.state.selectedStream
                  ? this.state.selectedStream
                  : null
              }
              onChange={(e) => this.calculateStreamResults(e.target.value)}
              className='block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              id='grid-state'
            >
              <option >Select Stream</option>
              <option value='MATHS'>Math</option>
              <option value='SCIENCE'>Bio Science</option>
              <option value='ART'>Art</option>
              <option value='COMMERCE'>Commerce</option>
            </select>
            <div className='pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700'>
              <svg
                className='fill-current h-4 w-4'
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 20 20'
              >
                <path d='M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z' />
              </svg>
            </div>
          </div>
        </div>
        <div className='table-v'>
        <table className='table-auto'>
          <thead>
            <tr>{this.state.tableHeads}</tr>
          </thead>
          <tbody>
              <tr>{this.state.tableRows}</tr>
              <tr>
                {this.state.selectedStream != null ? `stream Passes:  ${this.state.selectedStreamData.streamPasses} | stream Fails:  ${this.state.selectedStreamData.streamFails} ` : ''}
              </tr>
          </tbody>
          </table>  
        </div>
        
      </div>
    );
  }
}
