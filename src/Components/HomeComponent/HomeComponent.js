import React, { useState, useEffect } from 'react';
import * as _ from 'lodash';
import './HomeComponent.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faTrash,
  faCaretRight,
  faCaretLeft,
  faBars,
} from '@fortawesome/free-solid-svg-icons';
import AddRecord from '../AddRecord/AddRecord';
import Analytics from '../ShowAnalytics/Analytics';
import Swal from 'sweetalert2';
const { ipcRenderer } = window.require('electron');

const fs = window.require('fs');

export default function HomeComponent() {
  const [records, setRecords] = useState([]);
  const [dataSet, setDataSet] = useState([]);
  const [selectedYear, setYear] = useState('');
  const [analyticView, setAnalyticView] = useState(false);
  const [addRecordView, setAddRecordView] = useState(false);
  const [showPopupView, setPopupView] = useState();

  useEffect(() => {
    //  changeView()
    loadData(selectedYear);
  }, [selectedYear]);
  useEffect(() => {
    // changeView();
  }, [analyticView]);

  function changeView(id) {
    if (id === 1) {
      setAddRecordView(!addRecordView);
      setAnalyticView(false);
    } else {
      setAnalyticView(!analyticView);
      setAddRecordView(false);
    }
  }
  function renderData(data) {
    const items = data.map((i, key) => {
      const {
        _id: id,
        index,
        stream,
        name,
        attempt,
        sub1,
        grade1 = '+',
        grade2 = '+',
        grade3 = '+',
        sub2,
        sub3,
        universitySelected = false,
      } = i;
      return (
        <tr className='hover:bg-teal-300' key={id}>
          <td className='border px-4 py-2'>{index}</td>
          <td className='border px-4 py-2'>{name}</td>
          <td className='border px-4 py-2'>{stream}</td>
          <td className='border px-4 py-2'>{attempt}</td>
          <td className='border px-4 py-2'>{sub1}</td>
          <td className='border px-4 py-2'>{grade1}</td>
          <td className='border px-4 py-2'>{sub2}</td>
          <td className='border px-4 py-2'>{grade2}</td>
          <td className='border px-4 py-2'>{sub3}</td>
          <td className='border px-4 py-2'>{grade3}</td>
          <td className='border px-4 py-2'>{universitySelected ? 'YES' : 'NO'}</td>
          <td
            onClick={(e) => preDelete(id)}
            className='border px-4 py-2 hover:bg-red-800'
          >
            <FontAwesomeIcon icon={faTrash} />
          </td>
        </tr>
      );
    });
    setRecords(items);
  }
  function preDelete(id) {
    Swal.fire({
      text: 'Are you sure want to delete this record?',
      showCancelButton: true,
      confirmButtonText: 'Delete',
    }).then((e) => {
      if (e.isConfirmed) {
        deleteRecord(id);
      }
    });
  }

  function deleteRecord(id) {
    ipcRenderer.send('deleteRecords', id);
    ipcRenderer.on('deleteResponse', (event, data) => {
      loadData(selectedYear);
    });
  }

  function loadData(year) {
    ipcRenderer.send('getRecords', year);
    ipcRenderer.on('recordResponse', (event, payload) => {
      setDataSet(JSON.parse(payload));
      renderData(JSON.parse(payload));
    });
  }
  function reloadData() {
    loadData(selectedYear);
  }
function close() {
    setAddRecordView(false);
    setAnalyticView(false);
  
  }
  const tableView = (       <table className='table-auto bg-blue-200'>
  <thead className='bg-blue-500 shadow-2xl'>
    <tr>
      <th className='px-4 py-2'>Index</th>
      <th className='px-4 py-2'>Student Name</th>
      <th className='px-4 py-2'>Section</th>
      <th className='px-4 py-2'>Attempt</th>
      <th className='px-4 py-2'>Subject1</th>
      <th className='px-4 py-2'>Grade1</th>
      <th className='px-4 py-2'>Subject2</th>
      <th className='px-4 py-2'>Grade2</th>
      <th className='px-4 py-2'>Subject3</th>
      <th className='px-4 py-2'>Grade3</th>
      <th className='px-4 py-2'>University Selected</th>
    </tr>
  </thead>
  <tbody className='overflow-auto h-5 '>{records}</tbody>
</table>);
  return (
    <>
      <div className='sub-container '>
        <h1 className='flex justify-center p-5 font-bold'>
          {' '}
          Exam result {selectedYear}
        </h1>

        <div className='flex justify-center '>
          <div className='md:flex md:items-center mb-6 '>
            <div className='md:w-3/3'>
            </div>
            <div className=' p-5 md:w-3/3'>
              <select
                onChange={(e) => setYear(e.target.value)}
                value={selectedYear}
                className='block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
                id='grid-state'
              >
                <option>select year</option>
                <option value='2016'>2016</option>
                <option value='2017'>2017</option>
                <option value='2018'>2018</option>
                <option value='2019'>2019</option>
                <option value='2020'>2020</option>
                <option value='2021'>2021</option>
                <option value='2022'>2022</option>
                <option value='2023'>2023</option>
                <option value='2024'>2024</option>
                <option value='2025'>2025</option>
                <option value='2026'>2026</option>
                <option value='2027'>2027</option>
                <option value='2028'>2028</option>
                <option value='2029'>2029</option>
                <option value='2030'>2030</option>
                <option value='2031'>2031</option>
              </select>
            </div>
          </div>

          <button
            onClick={(e) => {
              changeView(1);
            }}
            className='bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-8 border-b-4 border-blue-700 hover:border-blue-500 rounded h-12 mt-5 mr-2'
          >
            Add records
          </button>
          <button
            onClick={(e) => {
              changeView(2);
            }}
            className='bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-8 border-b-4 border-blue-700 hover:border-blue-500 rounded h-12 mt-5'
          >
            Analytics View
          </button>
          {/* <FontAwesomeIcon onClick={() => { setSideView(!sideView) }} className='fixed ml-24 bar-icon' icon={sideView ? faCaretLeft : faCaretRight} /> */}
        </div>

        <div className='flex'>
          <div className='flex-1 text-gray-700 text-center px-4 py-2 m-2 md:w-3/3'>
     {records.length !==0? tableView: 'No data' }
          </div>
        </div>
      </div>
      <p className='floating-menu'>
        <ul className='floating-menu-items'>
          <li
            onClick={(e) => {
              changeView(1);
            }}
          >
            Add Record
          </li>
          <li
            onClick={(e) => {
              changeView(2);
            }}
          >
            Analytic View
          </li>

              </ul>
              <FontAwesomeIcon
            className='ml-24 floating-button'
            icon={faBars}
          />
      </p>

      <div className='flex '>
        {analyticView ? (
          <Analytics selectedYear={selectedYear} dataSet={dataSet} close={close} />
        ) : null}
        {addRecordView ? (
          <AddRecord selectedYear={selectedYear} refresh={reloadData} close={close} />
        ) : null}
      </div>
    </>
  );
}
