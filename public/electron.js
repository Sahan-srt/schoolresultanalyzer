
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');
const isDev = require('electron-is-dev');
let mainWindow;
const { init, addResult, findResult, deleteRecord } = require('./services/mongoService');
const { ipcMain } = require('electron');

function createWindow() {
    mainWindow = new BrowserWindow({ width: 900, height: 680, webPreferences: { nodeIntegration: true, webSecurity: false, } });
    mainWindow.loadURL(isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`);
    mainWindow.on('closed', () => mainWindow = null);
    init();
}
ipcMain.on('createRecord', async (event, payload) => {
    const res = await addResult(payload);
    event.sender.send('createResponse', JSON.stringify(res));
});
ipcMain.on('getRecords', async (event, year) => {
    const x = await findResult(year);
    event.sender.send('recordResponse', JSON.stringify(x));
});

ipcMain.on('deleteRecords', async (event, id) => {
    const x = await deleteRecord(id);
    event.sender.send('deleteResponse', JSON.stringify(x));
});

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});

