const mongoose = require('mongoose');
const Result = require('./Result');
module.exports = {
    async init() {
        //add db link here
        const uri = "";
        mongoose.connect(
            uri,
            { useNewUrlParser: true, useCreateIndex: true },
            error => {
                if (error) {
                    console.log('Error on connecting to MongoDB');
                } else {
                    console.log('Connected to MongoDB | Ready for use.');
                    // chalkAnimation.rainbow('Connected to MongoDB | Ready for use.', 0.4).start();
                }
            }
        );
    },
    async addResult(payload) {
        try {
            const x = await new Result(payload).save();
            return x;
        } catch (error) {
            console.log(error);

        }
    },
    async findResult(year) {
        try {
            const result = await Result.find({ year }).lean();
            return result;
        } catch (error) {
            console.log(error);

        }
    },

    async deleteRecord(id) {
        try {
            const result = await Result.findOneAndDelete({ _id: id });
            console.log(result);

            return result;
        } catch (error) {
            console.log(error);

        }
    }



}