const mongoose = require("mongoose");

const schema = new mongoose.Schema({
    index: {
        type: String,
        required: true
    },
    name: { type: String, required: true },
    attempt: { type: Number, required: true },
    stream: { type: String, required: true },
    sub1: { type: String, required: true },
    grade1: { type: String, required: true },
    sub2: { type: String, required: true },
    grade2: { type: String, required: true },
    sub3: { type: String, required: true },
    grade3: { type: String, required: true },
    year: { type: String, required: true },
    universitySelected: { type: Boolean, default:true },
}, {
    collection: 'results',
    timestamps: true,
});

module.exports = mongoose.model('Result', schema);